---
title: "Up and Running"
date: 2021-08-21T14:15:07-07:00
tags: ["github", "hugo", "cupper"]
---

Welcome to the blog!

I just got up and running using [GitLab pages][1], [Hugo][2], and the [Cupper theme][3]. It was pretty easy to do. Most
of my time was spent tweaking the config, just like when you first move into a house and are setting up a room. It was
a lot of moving items around trying to get it just right. One could create a site really fast; if you want to start a
blog of your own, I highly suggest it.

If you want to know more about me, checkout the [about page][4].

[1]: https://docs.gitlab.com/ee/user/project/pages/
[2]: https://gohugo.io/
[3]: https://github.com/zwbetz-gh/cupper-hugo-theme/
[4]: /blog/about/
