---
title: "Build Your Own Spreadsheet"
date: 2021-08-25T16:18:27-07:00
tags: ["8th Light", "Book Review", "Parsing", "Lexing", "TDD"]
---

{{< figure src="build_your_own_spreadsheet.png"
alt="Build Your Own Spreadsheet"
link="https://buildyourownspreadsheet.com"
height=400 width=310 >}}

## Spreadsheets and Computers
Computers and spreadsheets have a long history. Transitioning from the old bookkeeping ledgers of yesteryear, the
electronic spreadsheet was first outlined by Richard Mattessich in the paper [Budgeting Models and System Simulation][1]
in 1961. What started as batch processes producing spreadsheets written in languages like [FORTRAN][2] eventually paved
the way for spreadsheet classics like [Lotus 1-2-3][3]. Today, demand is still high for electronic spreadsheets
and those who are skilled at using them. One may even make the argument that the world still runs on spreadsheets. I
am willing to bet that you, dear reader, have used an electronic spreadsheet your self.

## Build Your Own Spreadsheet
Have you ever wondered what makes an electronic spreadsheet work? Well, that is exactly the question an [8th Light][4]
colleague, Christoph Gockel, asks in his new book, [*Build Your Own Spreadsheet*][5]. I recently had the pleasure to
work my way through this text, and it was pure delight. Christoph not only shows how the foundations of a spreadsheet
work, he also does so by driving the code test first.

For those just getting started in the world of software, [*Build Your Own Spreadsheet*][5] would be a great book you
could read shortly after learning your first language, or for anyone interested in learning more about test driven
development. If you are like me, and are already along in your career, it is equally engaging if you just want to learn
more about how spreadsheets work.

We begin creating a table of rows and columns in a browser and make it look like a spreadsheet. Soon you are enabling
cells to be edited, creating highlights, and adding keyboard shortcuts you would expect a functioning spreadsheet to 
utilize. The real fun begins when you implement a lexer and parser, including a parse tree, so you can support formulas.
The book sets a solid foundation for a working spreadsheet, and you are empowered to implement as many more features as
you wish.

For a self-published author, Christoph has done a great job. The text reads like a conversation, and if you follow
along step by step, you will create a basic 5x5 cell spreadsheet that runs in a web browser.

## Lexers & Parsers
For me, the lexer and parser inspired me to pick up [The Dragon Book][6] (*Compilers: Principles, Techniques, and
Tools*).

{{< figure src="dragon_book.png" alt="The Dragon Book"
link="https://www.pearson.com/us/higher-education/program/Aho-Compilers-Principles-Techniques-and-Tools-2nd-Edition/PGM167067.html"
height=300 width=200 >}}

The first few chapters cover these topics but in a lot of detail. I ended up finding a slightly more
approachable text, [Crafting Interpreters][7], that leads your through implementing an interpreter and a compiler for a
language calle lox.

{{< figure src="crafting_interpreters.png" alt="Crafting Interpreters"
link="https://craftinginterpreters.com"
height=300 width=200 >}}

I have already started, and have [a working scanner][8], and hope to share more soon.

[1]: https://www.jstor.org/stable/242869
[2]: https://fortran-lang.org
[3]: https://en.wikipedia.org/wiki/Lotus_1-2-3
[4]: https://8thlight.com
[5]: https://buildyourownspreadsheet.com
[6]: https://www.pearson.com/us/higher-education/program/Aho-Compilers-Principles-Techniques-and-Tools-2nd-Edition/PGM167067.html
[7]: https://craftinginterpreters.com
[8]: https://github.com/Aoxian/jlox/blob/63513798f63d205b0a27149023c675d571ae0296/src/main/java/net/kirogag/lox/Scanner.java
