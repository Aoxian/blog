---
date: "2021-08-21T14:37:34-07:00"
title: "Ian blogs here"
---

{{< fileTree >}}
**Latest Posts**:
  * [Build Your Own Spreadsheet](/blog/posts/build-your-own-spreadsheet)
  * [Up and Running](/blog/posts/up-and-running)
{{< /fileTree >}}

Welcome! This site is mainly for posting about my coding adventures. The code should be available in my public repos on
[GitHub][1].

[1]: https://github.com/Aoxian/
