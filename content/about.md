---
date: 2021-08-21T12:15:22-07:00
title: "About"
tags: ["8th light", "github", "hugo", "cupper"]
---

## Welcome to my blog!
Nice to meet you!

This site is mainly for posting about my coding adventures. The code should be available in my public repos on
[GitHub][1].

## All about Ian
Originally from Southwestern Pennsylvania, I traveled the world extensively in my 20s, and am currently dwelling in
Southern California.

I am a software crafter at [8th Light][2].

I like hiking, cooking and brewed beverages.

I used to be in a band, more like a few bands, actually many bands. I do not do that so much any more but I still like
to slap the bass.

I enjoy reading books and discussing them, especially with the folks at [OnlineGreatBooks][3], where I occasionaly host
seminars.

## Who is Aoxian?
A long time ago in an MMO far away called Final Fantasy 11, I was known as Aoxomoxoa from the Gilgamesh server. That
name was super hard to pronounce, so everyone just used to call me Aox [(e o ɛks)][4]

Eventually I grew out of the MMO lifestyle (or shall I say, lack of a lifestyle) and moved on to bigger more wonderful
things. The name was hard to keep as it is a [popular name][5]. Since my actual name is Ian, I decided to concatenate
the two.

## Aliases
Over the years I have used different names for different things. It can be confusing and for that I appologize. This
brief codex should clear things up for the curious. One day, I may rebrand, though I do not know as what.

**kirogag**: kirogag.net is the domain I sometimes use. It is my last name, backwards. I thought it was clever at the time.

**ianaox**: Depending on where you look, I sometimes go by ianaox because usernames have to be unique and believe it
or not, aoxian was already taken. If you "found me", it may actually be someone else. Do not assume, always double
check.

**~laclur-piddyt**: I have a planet, this is its name. If you don't know what this means, may I suggest checking out
[martian computing][6].

[1]: https://github.com/Aoxian=/
[2]: https://8thlight.com/
[3]: https://onlinegreatbooks.com/
[4]: https://easypronunciation.com/en/american-english-pronunciation-ipa-chart#american_english_vowels
[5]: https://en.wikipedia.org/wiki/Aoxomoxoa
[6]: https://davis68.github.io/martian-computing/
